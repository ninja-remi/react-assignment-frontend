import axios from 'axios'
import {configs} from '../config'
import {generateSignature} from '../helpers'

export const bankSwiftIbanLookUp = (data) => {

    let headers = { 
        "client-id": configs.CLIENT_ID,
        signature: generateSignature(data)
    }

    return axios.post(
        configs.BASE_URL + "/v1/banks/find",
        data,
        {headers: headers}
    )
}