import {configs} from '../config'
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';
 

export const generateSignature = (data) => {
    const SECRET = configs.SECRET_KEY
 
    let body = JSON.stringify(data);

    return Base64.stringify(hmacSHA512(body, SECRET)); 

}