import React from 'react';
import BankingForm from './components/BankingForm'

import './assets/scss/app.scss'

function App() {
  return (
    <div className="container">
      <BankingForm />
    </div>
  );
}

export default App;
