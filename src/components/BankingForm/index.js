import React, { useState } from "react";
import { debounce } from 'throttle-debounce'; 
import { bankSwiftIbanLookUp } from "../../services";

const BankingForm = () => {
  const [formData, setFormData] = useState({
    bankCountry: "",
    currency: "",
    methodType: "swift",
    swiftIBAN: "",
    accountNumber: "",
    confirmAccountNumber: "",
    bankDetails: "",
    loader: false,
    errors: {
      bankCountry: "",
      currency: "",
      methodType: "",
      swiftIBAN: "",
      accountNumber: "",
      confirmAccountNumber: "",
    },
  });

  const handleState = (e) => {
    setFormData({
      ...formData,
      errors: { 
        ...formData.errors,
        [e.target.name]: ""
      },
      [e.target.name]: e.target.value,
    });
  };

  const handleCurrency = (e) => {
    let currency = "";

    if (e.target.value === "UK") {
      currency = "GBP";
    }

    if (e.target.value === "DE") {
      currency = "EUR";
    }

    setFormData({
      ...formData,
      errors: {
        ...formData.errors,
        [e.target.name]: "",
        currency:""
      },
      [e.target.name]: e.target.value,
      currency,
    });
  };

  const handleRadio = (e) => {  
    setFormData({
      ...formData,
      errors: {
        ...formData.errors,
        [e.target.name]: "",
        swiftIBAN:"",
        accountNumber:"",
        confirmAccountNumber:""    
      },
      [e.target.name]: e.target.value,
      bankDetails:"",
      swiftIBAN:"",
      accountNumber:"",
      confirmAccountNumber:"" 
    });
  };

  const handleSwiftIbanLookupAPI = (e) => {
 

    setFormData({
        ...formData,
        errors: {
            ...formData.errors,
            [e.target.name]: "",
        },
        [e.target.name]: e.target.value,
        loader: true
    }); 
  
    bankSwiftIbanLookUp({
        swift_bic: e.target.value,
      })
      .then((res) => {
        if (res.status === 200) {
          setFormData({
            ...formData,
            bankDetails: res.data,
            loader: false,
            swiftIBAN:e.target.value,
            errors: {
                ...formData.errors,
                swiftIBAN: "",
            },
          });
        } else {
          setFormData({
            ...formData,
            bankDetails: "",
            loader: false,
            errors: {
              ...formData.errors,
              swiftIBAN: res.data.message[0],
            },
            swiftIBAN:e.target.value
          });
        }
      })
      .catch((err) => {
        setFormData({
          ...formData,
          bankDetails: "",
          loader: false,
          errors: {
            ...formData.errors,
            swiftIBAN: "Invalid CODE entered.",
          },
          swiftIBAN:e.target.value
        });
      });
  };
 
  const handleSwiftIbanLookupDebounce = debounce(1000, handleSwiftIbanLookupAPI);

  const handleSwiftIbanLookup = (e) => {   
    handleSwiftIbanLookupDebounce({
        target:{
            name: e.target.name,
            value: e.target.value
        }
    });
  };

  const handleValidation = () => {
    let errors = { ...formData.errors };
    if (formData.bankCountry === "") {
      errors.bankCountry = "Please Select Bank Country";
    }

    if (formData.currency === "") {
      errors.currency = "Please Select Currency";
    }

    if (formData.swiftIBAN === "") {
      errors.swiftIBAN = "Enter Swift/IBIN Number";
    }

    if (formData.swiftIBAN === "") {
      if (formData.methodType === "swift") {
        errors.swiftIBAN = "Enter Swift Number";
      } else {
        errors.swiftIBAN = "Enter IBIN Number";
      }
    }

    if (formData.methodType === "swift" && formData.accountNumber === "") {
      errors.accountNumber = "Enter Account Number";
    }

    if (
      formData.methodType === "swift" &&
      formData.confirmAccountNumber !== formData.accountNumber
    ) {
      errors.confirmAccountNumber = "Confirm Account Not Matched";
    }

    if (
      formData.methodType === "swift" &&
      formData.confirmAccountNumber === ""
    ) {
      errors.confirmAccountNumber = "Enter Confirm Account Number";
    } 
    setFormData({
      ...formData,
      errors,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    handleValidation();
  };

  return (
    <div className="banking-form">
      <form onSubmit={handleSubmit.bind(this)}>
        <div className="row">
          <div className="col-4">
            <h2>Banking Information</h2>
            <p>Lorem ipsum dolar smith</p>
          </div>
          <div className="col-8">
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="bankCountry">Bank Country</label>
                  <select
                    id="bankCountry"
                    name="bankCountry"
                    className="form-control"
                    onChange={(e) => {
                      handleCurrency(e);
                    }}
                    defaultValue={formData.bankCountry}
                  >
                    <option value="">Select Country</option>
                    <option value="UK">United Kingdom</option>
                    <option value="DE">Germany</option>
                  </select>
                </div>
                {formData.errors.bankCountry && (
                  <div className="alert alert-danger">
                    {formData.errors.bankCountry}
                  </div>
                )}
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="currency">Currency</label>
                  <select
                    id="currency"
                    className="form-control"
                    name="currency"
                    onChange={(e) => {
                      handleState(e);
                    }}
                    defaultValue={formData.currency}
                  >
                    <option value="">Select Currency</option>
                    <option value="GBP" selected={formData.currency==="GBP"}>GBP</option>
                    <option value="EUR" selected={formData.currency==="EUR"}>EUR</option>
                  </select>
                </div>
                {formData.errors.currency && (
                  <div className="alert alert-danger">
                    {formData.errors.currency}
                  </div>
                )}
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="method">Select Method</label>
                  <div
                    className="btn-group btn-group-toggle d-block method-btn"
                    data-toggle="buttons"
                  >
                    <label
                      className={`btn ${
                        formData.methodType === "swift" && "active"
                      }`}
                    >
                      <input
                        type="radio"
                        name="methodType"
                        id="method1"
                        value="swift"
                        onClick={(e) => {
                          handleRadio(e); 
                        }}
                      />{" "}
                      SWIFT
                    </label>
                    <label
                      className={`btn ${
                        formData.methodType === "ibin" && "active"
                      }`}
                    >
                      <input
                        type="radio"
                        name="methodType"
                        id="method2"
                        value="ibin"
                        onClick={(e) => {
                            handleRadio(e);
                        }}
                      />{" "}
                      IBAN
                    </label>
                  </div>
                </div>
              </div>
              <div className="col-6"></div>
            </div>
            <div className="row">
              <div className="col-6">
                <div className={`form-group ${formData.loader ? "loader" : null}`}>
                  <label htmlFor="swiftIBAN">
                    {formData.methodType === "swift" ? "SWIFT" : "IBIN"}
                  </label>
                  <input
                    type="text"
                    name="swiftIBAN"
                    className="form-control"
                    id="swiftIBAN"
                    placeholder=""
                    onChange={(e) => {
                      handleSwiftIbanLookup(e);
                    }} 
                    autoComplete="off"
                  />
                </div>

                {formData.errors.swiftIBAN && (
                  <div className="alert alert-danger">
                    {formData.errors.swiftIBAN}
                  </div>
                )}
                {formData.bankDetails && (
                  <div class="card form-group">
                    <div class="card-body">
                      <p class="card-text">
                        {formData.bankDetails.bank_name},{" "}
                        {formData.bankDetails.bank_address1},{" "}
                        {formData.bankDetails.bank_address2},{" "}
                        {formData.bankDetails.bank_city},{" "}
                        {formData.bankDetails.bank_state_province},{" "}
                        {formData.bankDetails.bank_country},{" "}
                        {formData.bankDetails.bank_postal_code}
                      </p>
                    </div>
                  </div>
                )}
              </div>
              <div className="col-6"></div>
            </div>
            {formData.methodType === "swift" && (
              <div className="row">
                <div className="col-6">
                  <div className="form-group">
                    <label htmlFor="accountNumber">Account Number</label>
                    <input
                      type="text"
                      name="accountNumber"
                      className="form-control"
                      id="accountNumber"
                      placeholder=""
                      onChange={(e)=>{handleState(e)}}
                    />
                  </div>

                  {formData.errors.accountNumber && (
                    <div className="alert alert-danger">
                      {formData.errors.accountNumber}
                    </div>
                  )}
                </div>
                <div className="col-6">
                  <div className="form-group">
                    <label htmlFor="confirmAccountNumber">
                      Confirm Account Number
                    </label>
                    <input
                      type="text"
                      name="confirmAccountNumber"
                      className="form-control"
                      id="confirmAccountNumber"
                      placeholder="" 
                      onChange={(e)=>{handleState(e)}}
                    />
                  </div>

                  {formData.errors.confirmAccountNumber && (
                    <div className="alert alert-danger">
                      {formData.errors.confirmAccountNumber}
                    </div>
                  )}
                </div>
              </div>
            )}
            <div className="row">
              <div className="col">
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default BankingForm;
